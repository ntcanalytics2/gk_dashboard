#!/usr/bin/env python
# coding: utf-8

# In[33]:


import pandas as pd
import numpy as np
import gspread

from oauth2client.service_account import ServiceAccountCredentials

scope= ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']

creds= ServiceAccountCredentials.from_json_keyfile_name('jupyter_spreadsheet_joiner.json',scope)

client= gspread.authorize(creds)

spreadsheet_key= '17h33Brr8D4OT0WgP26To0ToNa9i7I9t2SRYf9NlNH2c'

sheet_list_all_courses = client.open("Delhi Happiness Mahotsav").worksheet('List all courses')

table_list_all_courses = sheet_list_all_courses.get_all_values()
delhi_hm_list_all_courses= pd.DataFrame(table_list_all_courses[1:], columns=table_list_all_courses[0])
delhi_hm_list_all_courses.rename(columns={'ID':'Course ID'}, inplace=True)

delhi_hm_list_all_courses_selected = delhi_hm_list_all_courses[['Course ID','Course Type',
                                    'Teachers','District', 'Start Date', 'End Date','Phone']]

sheet_upcoming= client.open("Delhi Happiness Mahotsav").worksheet('Upcoming Courses')

table_upcoming = sheet_upcoming.get_all_values()

delhi_hm_upcoming= pd.DataFrame(table_upcoming[1:], columns=table_upcoming[0])
delhi_hm_upcoming

PAX_df= delhi_hm_upcoming[['Course ID', 'Count of Registrations']]
PAX_df = PAX_df.replace(r'^\s*$', np.nan, regex=True)

PAX_df = PAX_df.fillna(method='ffill')
PAX_df_sorted = PAX_df[PAX_df['Count of Registrations'].str.contains('Total Count')]
PAX_df_sorted.rename(columns={'Count of Registrations':'PAX'}, inplace=True)

delhi_hm = delhi_hm_list_all_courses_selected.merge(PAX_df_sorted, on=['Course ID'])
delhi_hm['PAX']= delhi_hm['PAX'].str.replace('Total Count : ', '')
delhi_hm

delhi_hm['Last Updated']= pd.Timestamp("today").strftime("%m/%d/%Y")
delhi_hm

# delhi_hm['Teacher Count'] = delhi_hm['Teachers'].apply(lambda x: len(x.split(',')))
# delhi_hm['Teacher Count'].sum()

Teachers = delhi_hm['Teachers'].str.split(',', expand=True).values.ravel()

ID = np.repeat(delhi_hm['Course ID'].values, len(Teachers) / len(delhi_hm))
Course_Type = np.repeat(delhi_hm['Course Type'].values, len(Teachers) / len(delhi_hm))
District = np.repeat(delhi_hm['District'].values, len(Teachers) / len(delhi_hm))
Start_Date = np.repeat(delhi_hm['Start Date'].values, len(Teachers) / len(delhi_hm))
End_Date = np.repeat(delhi_hm['End Date'].values, len(Teachers) / len(delhi_hm))
Phone = np.repeat(delhi_hm['Phone'].values, len(Teachers) / len(delhi_hm))
PAX = np.repeat(delhi_hm['PAX'].values, len(Teachers) / len(delhi_hm))
Last_Updated = np.repeat(delhi_hm['Last Updated'].values, len(Teachers) / len(delhi_hm))

delhi_hm_df = pd.DataFrame({'Course ID':ID,'Course Type':Course_Type,'Teachers':Teachers,
                            'District':District,'Start Date':Start_Date,'End Date':End_Date,
                            'Phone':Phone,'PAX':PAX,'Last Updated':Last_Updated})
delhi_notnull_df = delhi_hm_df[delhi_hm_df['Teachers'].notnull()]

delhi_notnull_df['Teacher'], delhi_notnull_df['Teacher Code'] = delhi_notnull_df['Teachers'].str.split('(', 1).str
delhi_notnull_df['Teacher Code'] = delhi_notnull_df['Teacher Code'].str.replace(')', '')

delhi_notnull_df['Teacher']= delhi_notnull_df['Teacher'].str.upper().str.title()
delhi_notnull_df['Teacher'] = delhi_notnull_df['Teacher'].str.lstrip()
delhi_notnull_df['Teacher Code'] = delhi_notnull_df['Teacher Code'].str.lstrip()

delhi_notnull_df['Start Date']=pd.to_datetime(delhi_notnull_df['Start Date'])
delhi_notnull_df['End Date']=pd.to_datetime(delhi_notnull_df['End Date'])
delhi_notnull_df

delhi_notnull_df['Last Updated']= delhi_notnull_df['Last Updated'].astype('datetime64[ns]')

delhi_notnull_df.drop_duplicates(subset=['Course ID','Course Type','District',
        'Start Date', 'End Date', 'Phone','PAX',
        'Teacher', 'Teacher Code'], keep='last', inplace=True)

spreadsheet_key = '1mqZMAIwBs4N37tznn-yvsv_R56LitS1lH6lMzEMyoH4'
from df2gspread import df2gspread as d2g
wks_name = 'Happiness Mahotsav'
d2g.upload(delhi_notnull_df, spreadsheet_key, wks_name, credentials=creds, row_names=True)

delhi_notnull_df['Last Updated'] =  delhi_notnull_df['Last Updated'].astype(str)

delhi_2019_with_PAX = delhi_2019_df[delhi_2019_df['PAX'].notnull()]
delhi_2019_without_PAX = delhi_2019_df[delhi_2019_df['PAX'].isnull()]

delhi_hm_2020 = delhi_notnull_df[['Course ID', 'Course Type','District','Teacher', 
                'Teacher Code','Start Date', 'End Date','PAX','Last Updated']]

unique_PAXonly_df = delhi_2019_with_PAX[~delhi_2019_with_PAX['Teacher Code'].isin(delhi_hm_2020['Teacher Code'])]

combined_delhi_PAX_df = pd.concat([unique_PAXonly_df, delhi_notnull_df], ignore_index=True, sort=False)
# combined_delhi_PAX_df['Teacher Code'].nunique()

unique_without_PAX_df = delhi_2019_without_PAX[~delhi_2019_without_PAX['Teacher Code'].isin(combined_delhi_PAX_df['Teacher Code'])]

unique_without_PAX_df

combined_delhi_df = pd.concat([unique_without_PAX_df, combined_delhi_PAX_df], ignore_index=True, sort=False)
combined_delhi_df

combined_delhi_df = combined_delhi_df[['ID', 'Course Type', 'City', 'District', 'Start Date',
       'End Date', 'Teacher', 'Teacher Code', 'PAX', 'Last Updated']]

combined_delhi_df

last_updated_time = combined_delhi_df['Last Updated'].iloc[-1]

combined_delhi_df['Last Updated all'] = last_updated_time

combined_delhi_df['Teacher Code'] = combined_delhi_df['Teacher Code'].str.lstrip()

combined_delhi_df = combined_delhi_df.fillna('')

# combined_delhi_df.to_csv('combined_delhi_df.csv')

spreadsheet_key = '1mqZMAIwBs4N37tznn-yvsv_R56LitS1lH6lMzEMyoH4'
from df2gspread import df2gspread as d2g
wks_name = 'Delhi Combined'
d2g.upload(combined_delhi_df, spreadsheet_key, wks_name, credentials=creds, row_names=True)


# In[ ]:





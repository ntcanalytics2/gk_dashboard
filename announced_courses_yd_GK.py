#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import gspread

from oauth2client.service_account import ServiceAccountCredentials

scope= ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']

creds= ServiceAccountCredentials.from_json_keyfile_name('jupyter_spreadsheet_joiner.json',scope)

client= gspread.authorize(creds)

spreadsheet_key= '12DXqt1MCIhFxv3v2TNrX4VZEfBIEczKVqG4XsHAH3G4'

sheet_list_all_courses = client.open("Announced Courses").worksheet('List all courses')

table_list_all_courses = sheet_list_all_courses.get_all_values()
announced_list_all_courses= pd.DataFrame(table_list_all_courses[1:], columns=table_list_all_courses[0])
announced_list_all_courses.rename(columns={'ID':'Course ID'}, inplace=True)

announced_list_all_courses_selected = announced_list_all_courses[['Course ID','Course Type',
                                    'Teachers','District', 'Start Date', 'End Date','Phone']]

sheet_upcoming= client.open("Announced Courses").worksheet('Upcoming Courses')

table_upcoming = sheet_upcoming.get_all_values()

announced_upcoming= pd.DataFrame(table_upcoming[1:], columns=table_upcoming[0])
announced_upcoming

PAX_df= announced_upcoming[['Course ID', 'Count of Registrations']]
PAX_df = PAX_df.replace(r'^\s*$', np.nan, regex=True)

PAX_df = PAX_df.fillna(method='ffill')
PAX_df_sorted = PAX_df[PAX_df['Count of Registrations'].str.contains('Total Count')]
PAX_df_sorted.rename(columns={'Count of Registrations':'PAX'}, inplace=True)

announced_PAX_merged = announced_list_all_courses_selected.merge(PAX_df_sorted, on=['Course ID'])
announced_PAX_merged

spreadsheet_key_dist_SYC = '1Bj8ZjG3QrCzJDwBOb9UXHPC_tIgiG3zFviYxbNHllew'

sheet_dist_SYC = client.open("District corresponding with SYC").sheet1

table_dist_SYC = sheet_dist_SYC.get_all_values()

state_dist_syc_df= pd.DataFrame(table_dist_SYC[1:], columns=table_dist_SYC[0])
state_dist_syc_df.drop_duplicates(subset=['District','State','SYC'], inplace=True)

# announced_courses_df['Teacher Count'] = announced_courses_df['Teachers'].apply(lambda x: len(x.split(',')))

# announced_courses_df['Teacher Count'].sum()

Teachers = announced_courses_df['Teachers'].str.split(',', expand=True).values.ravel()

Course_ID = np.repeat(announced_courses_df['Course ID'].values, len(Teachers) / len(announced_courses_df))
Course_Type = np.repeat(announced_courses_df['Course Type'].values, len(Teachers) / len(announced_courses_df))
District = np.repeat(announced_courses_df['IC'].values, len(Teachers) / len(announced_courses_df))
Start_Date = np.repeat(announced_courses_df['Start Date'].values, len(Teachers) / len(announced_courses_df))
End_Date = np.repeat(announced_courses_df['End Date'].values, len(Teachers) / len(announced_courses_df))
Phone = np.repeat(announced_courses_df['Phone'].values, len(Teachers) / len(announced_courses_df))
PAX = np.repeat(announced_courses_df['PAX'].values, len(Teachers) / len(announced_courses_df))

announced_1per_row_df = pd.DataFrame({'Course ID':Course_ID,'Course Type':Course_Type,'Teachers':Teachers,
                            'District':District,'Start Date':Start_Date,'End Date':End_Date,
                                     'Phone':Phone,'PAX':PAX})

announced_1per_row_df = announced_1per_row_df[announced_1per_row_df['Teachers'].notnull()]

announced_1per_row_df['Start Date']=pd.to_datetime(announced_1per_row_df['Start Date'])
announced_1per_row_df['End Date']=pd.to_datetime(announced_1per_row_df['End Date'])

announced_1per_row_df['District']= announced_1per_row_df['District'].str.upper().str.title()
announced_1per_row_df.loc[announced_1per_row_df['Course ID']=='Q35000','District']='Thiruvananthapuram'

announced_1per_row_df = announced_1per_row_df.merge(state_dist_syc_df, on=['District'])

announced_1per_row_df.loc[announced_1per_row_df['District']=='Faridabad','State']='Delhi'
announced_1per_row_df.loc[announced_1per_row_df['District']=='Gautam Buddha Nagar','State']='Delhi'
announced_1per_row_df.loc[announced_1per_row_df['District']=='Gurgaon','State']='Delhi'
announced_1per_row_df.loc[announced_1per_row_df['District']=='Noida','State']='Delhi'
announced_1per_row_df.loc[announced_1per_row_df['District']=='Ghaziabad','State']='Delhi'

announced_1per_row_df.loc[announced_1per_row_df['State']=='Chandigarh','State']='Punjab'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Pondicherry','State']='Tamil Nadu'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Dadra and Nagar Haveli','State']='Gujarat'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Daman and Diu','State']='Gujarat'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Sikkim','State']='West Bengal'

announced_1per_row_df.loc[announced_1per_row_df['State']=='Arunachal Pradesh','State']='North East'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Assam','State']='North East'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Manipur','State']='North East'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Meghalaya','State']='North East'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Nagaland','State']='North East'
announced_1per_row_df.loc[announced_1per_row_df['State']=='Tripura','State']='North East'

sort_state = announced_1per_row_df['State'].unique()

announced_1per_row_df.loc[announced_1per_row_df['Course Type']=='3 Days YES!+','Course Type']='YES!+'
announced_1per_row_df.loc[announced_1per_row_df['Course Type']=='Happiness Program For Youth','Course Type']='HPY'

announced_1per_row_df.drop_duplicates(subset=['Course ID', 'Course Type', 'Teachers',
                                           'Start Date', 'PAX', 'SYC'], keep='last', inplace=True)

announced_1per_row_df['Teacher'], announced_1per_row_df['Teacher Code'] = announced_1per_row_df['Teachers'].str.split('(', 1).str
announced_1per_row_df['Teacher Code'] = announced_1per_row_df['Teacher Code'].str.replace(')', '')

announced_1per_row_df['Teacher']= announced_1per_row_df['Teacher'].str.upper().str.title()

announced_1per_row_df['Teacher'] = announced_1per_row_df['Teacher'].str.lstrip()
announced_1per_row_df['Teacher Code'] = announced_1per_row_df['Teacher Code'].str.lstrip()

announced_1per_row_df = announced_1per_row_df.replace('nan',"")

announced_1per_row_df['PAX']= announced_1per_row_df['PAX'].str.replace('Total Count : ', '')
announced_1per_row_df

announced_new_unique = announced_1per_row_df['Course ID'].unique().tolist()

announced_old_unique = announced_courses_df['Course ID'].unique().tolist()

left_courseID = list(set(announced_old_unique) - set(announced_new_unique))
left_courseID

spreadsheet_key = '1K2V_v5UcbhLGlfqub2ZiOEeoHpobOjnXSbzZEse78JY'
from df2gspread import df2gspread as d2g
wks_name = 'All State'
d2g.upload(announced_1per_row_df, spreadsheet_key, wks_name, credentials=creds, row_names=True)

sort_state = announced_1per_row_df['State'].unique()
state_1 = sorted(sort_state)
# state_1

from df2gspread import df2gspread as d2g
for i in state_1:
    announced_1per_row_df_i = announced_1per_row_df[announced_1per_row_df['State']== i]
    wks_name = i
    d2g.upload(announced_1per_row_df_i, spreadsheet_key, wks_name, credentials=creds, row_names=True)


# In[ ]:




